# Requriements

Install Expo
`npm i -g expo-cli`

Install dependencies
`yarn`

# Starting the app

Run with expo
`yarn start`

# Tests

`yarn test`