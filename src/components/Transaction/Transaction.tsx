import React from 'react'
import { StyleSheet, View, Text, ViewStyle, TouchableOpacity, Image } from 'react-native'
import { Transaction as ITransaction } from '../../types/Transaction'
import { CURRENCY } from '../../utils/currency'
import DynamicImage from '../common/DynamicImage/DynamicImage'

export interface Props {
    transaction: ITransaction
    containerStyle?: ViewStyle
    onPress?: () => void
}

export const Transaction = ({ transaction, onPress, containerStyle }: Props) => (
    <TouchableOpacity style={[styles.container, containerStyle]} onPress={onPress}>
        <View style={{ width: 50, height: 50 }}>
            <DynamicImage
                uri={transaction.icon}
                width={50}
                height={50}
            />
        </View>
        <View>
            <Text>{transaction.name}</Text>
            <Text>{`${CURRENCY[transaction.currency]}${transaction.amount}`}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                {/* TODO: show an icon for CF or improve UI + copy */}
                <Text>CF: {transaction.carbon_footprint}</Text>
                {/* TODO: Parse and himanise date */}
                <Text>{transaction.created_at}</Text>
            </View>
        </View>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    container: {
        borderColor: 'black',
        borderWidth: 1,
        width: '100%',
        padding: 5,
    },
})