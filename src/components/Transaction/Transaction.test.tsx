import { Transaction, Props as TransactionProps } from '.'
import { transactionsMock } from '../../mocks/transactions'
import { fireEvent, render, act, waitFor } from '@testing-library/react-native'

jest.mock('react-native-svg', () => ({
    SvgUri: () => <></>
}))

const renderComponent = (props?: Partial<TransactionProps>) => render(
    <Transaction transaction={transactionsMock.transactions[0]} {...props} />
)

describe('Transaction', () => {
    const pressMock = jest.fn()
    it('should render correctly', () => {
        const { toJSON } = renderComponent()

        expect(toJSON()).toMatchSnapshot()
    })
    it('should fire event on press', async () => {
        const { getByText } = renderComponent({ onPress: pressMock })

        act(
            () => fireEvent.press(getByText(transactionsMock.transactions[0].created_at))
        )

        await waitFor(() => {
            expect(pressMock).toHaveBeenCalledTimes(1)
        })
    })
})