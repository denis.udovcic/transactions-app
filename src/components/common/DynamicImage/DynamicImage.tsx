import React from 'react'
import { SvgUri } from 'react-native-svg'
import { Image } from 'react-native'

const isSvg = (str: string) => str.endsWith('.svg')

interface Props {
    uri: string
    width?: number
    height?: number
}

export default ({ uri, width = 50, height = 50 }: Props) => isSvg(uri)
    // Figure out a fallback for when svg/img can't be fetched
    ? <SvgUri
        width={width}
        height={height}
        uri={uri}
        color="#900"
    />
    : <Image
        source={{ uri }}
        width={width}
        height={height}
        resizeMode="cover"
        style={{ width: width, height: height }}
    />