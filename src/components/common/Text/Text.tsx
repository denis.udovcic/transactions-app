import React from 'react'
import { Text, StyleSheet } from 'react-native'

interface Props {
    children: string | number
    type?: 'primary' | 'secondary'
}

export default ({ children, type = 'primary' }: Props) =>
    <Text style={styles[type]}>{children}</Text>

const styles = StyleSheet.create({
    primary: {
        fontSize: 12,
    },
    secondary: {
        opacity: 0.8,
        fontSize: 10,
    }
})