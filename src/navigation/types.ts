import { routes } from "./routes";

export type NavigatorParamsList = {
    // ideally this would be some kind of hash or id
    [routes.transaction]: {
        transactionName: string
        createdAt: string
    },
    [routes.transactions]: {} | undefined,
}