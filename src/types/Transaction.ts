type Status = 'completed'
type IBoolean = 'TRUE' | 'FALSE' | ''
type TransactionType = 'card' | 'transfer'
type Currency = 'GBP'

export interface Transaction {
    "name": string
    "status": Status
    "category": string
    "carbon_footprint": number | ''
    "fees": number
    "type": TransactionType
    "amount": number
    "currency": Currency
    "created_at": string
    "icon": string
    "brand_partner": IBoolean
}