export enum CURRENCY {
    GBP = '£'
}

export const toCurrencyString = (value: number, currency = 'GBP' as 'GBP') => `${CURRENCY[currency]}${value}`