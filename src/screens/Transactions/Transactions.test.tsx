import { Transactions, Props as TransactionsProps } from './Transactions'
import { transactionsMock } from '../../mocks/transactions'
import { fireEvent, render, act, waitFor } from '@testing-library/react-native'
import { stores } from '../../store'

jest.mock('react-native-svg', () => ({
    SvgUri: () => <></>
}))
jest.mock('@react-native-async-storage/async-storage', () => ({
    getItem: jest.fn(async () => { }),
    setItem: jest.fn(),
}))

const renderComponent = (props: TransactionsProps) => render(
    <Transactions {...props} />
)

describe('screens/Transactions', () => {
    const navigateMock = jest.fn()
    const props: TransactionsProps = {
        transactions: stores.transactions,
        navigation: {
            navigate: navigateMock,
        } as any,
        route: {} as any,
    }
    beforeAll(() => {
        stores.transactions.setTransactions(transactionsMock.transactions)
    })
    it('should navigate to correct route', async () => {
        const { getByText } = renderComponent(props)

        act(
            () => fireEvent.press(getByText(transactionsMock.transactions[0].created_at))
        )

        await waitFor(() => {
            expect(navigateMock).toHaveBeenCalledWith(
                'transaction',
                {
                    'createdAt': '2021-04-22 02:15',
                    'transactionName': 'Tuition Fees',
                }
            )
        })
    })
})