import { inject, observer } from 'mobx-react'
import React, { useEffect, useCallback } from 'react'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { View, FlatList, StyleSheet } from 'react-native'
import { Transaction } from '../../components/Transaction'
import { NavigatorParamsList } from '../../navigation/types'
import TransactionStore from '../../store/transactions'
import { Transaction as ITransaction } from '../../types/Transaction'
import { routes } from '../../navigation/routes'

export type Props = {
    transactions: TransactionStore
} & NativeStackScreenProps<NavigatorParamsList, typeof routes.transactions>

export const Transactions = ({ transactions, navigation }: Props) => {
    useEffect(() => {
        transactions.fetchTransactions()
    }, [])

    const onTransactionPress = useCallback((tx: ITransaction) => {
        navigation.navigate('transaction', {
            transactionName: tx.name,
            createdAt: tx.created_at,
        })
    }, [navigation])

    return (
        <View style={styles.container}>
            <View style={styles.listWrapper}>
                <FlatList keyExtractor={(_, idx) => idx.toString()} data={transactions.transactions} renderItem={({ item }) => (
                    <Transaction onPress={() => onTransactionPress(item)} transaction={item} containerStyle={styles.transactionWrapper} />
                )} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 20,
        paddingHorizontal: 5,
    },
    listWrapper: {
        width: '100%',
    },
    transactionWrapper: {
        marginVertical: 5,
        flex: 1,
    }
});

export default inject('transactions')(observer(Transactions))