import React, { useMemo } from 'react'
import { inject, observer } from 'mobx-react'
import { View, StyleSheet } from 'react-native'
import DynamicImage from '../../components/common/DynamicImage/DynamicImage'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { toCurrencyString } from '../../utils/currency'
import TransactionStore from '../../store/transactions'
import { NavigatorParamsList } from '../../navigation/types'
import { routes } from '../../navigation/routes'
import Text from '../../components/common/Text/Text'

export type Props = {
    transactions: TransactionStore
} & NativeStackScreenProps<NavigatorParamsList, typeof routes.transaction>

export const Transaction = ({ transactions, route }: Props) => {
    const transaction = useMemo(() => transactions.transactions
        .find((tx) => tx.created_at == route.params.createdAt && tx.name === route.params.transactionName),
        [transactions, route]
    )

    if (!transaction) {
        // TODO: bettern handle when not found
        return null
    }

    return (
        <View style={styles.container}>
            <Text>{transaction.name}</Text>
            <View style={{ marginVertical: 20, width: 50, height: 50 }}>
                <DynamicImage
                    uri={transaction.icon}
                    width={50}
                    height={50}
                />
            </View>
            <View>
                <View style={{ marginVertical: 5 }}>
                    <Text>Amount</Text>
                    <Text type="secondary">{toCurrencyString(transaction.amount, transaction.currency)}</Text>
                </View>
                {/* TODO: show an icon for CF or improve UI + copy */}
                <View style={{ marginVertical: 5 }}>
                    <Text>Carbon footrpint</Text>
                    <Text type="secondary">{transaction.carbon_footprint}</Text>
                </View>
                {/* TODO: Parse and himanise date */}
                <View style={{ marginVertical: 5 }}>
                    <Text>Created at</Text>
                    <Text type="secondary">{transaction.created_at}</Text>
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text>Fees</Text>
                    <Text type="secondary">{toCurrencyString(transaction.fees, transaction.currency)}</Text>
                </View>
                <View style={{ marginVertical: 5 }}>
                    <Text>Category</Text>
                    <Text type="secondary">{transaction.category}</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 5,
    },
    listWrapper: {
        width: '100%',
    },
    transactionWrapper: {
        marginVertical: 5,
        flex: 1,
    },
});

export default inject('transactions')(observer(Transaction))