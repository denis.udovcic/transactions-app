import { Transaction, Props as TransactionProps } from './Transaction'
import { transactionsMock } from '../../mocks/transactions'
import { render } from '@testing-library/react-native'
import { stores } from '../../store'

jest.mock('react-native-svg', () => ({
    SvgUri: () => <></>
}))
jest.mock('@react-native-async-storage/async-storage', () => ({
    getItem: jest.fn(async () => {}),
    setItem: jest.fn(),
}))

const renderComponent = (props: TransactionProps) => render(
    <Transaction {...props} />
)

describe('screens/Transaction', () => {
    beforeAll(() => {
        stores.transactions.fetchTransactions = jest.fn()
        stores.transactions.setTransactions(transactionsMock.transactions)
    })
    const props: TransactionProps = {
        transactions: stores.transactions,
        navigation: {} as any,
        route: {
            params: {
                transactionName: transactionsMock.transactions[0].name,
                createdAt: transactionsMock.transactions[0].created_at,
            }
        } as any,
    }
    it('should render correctly', () => {
        const { toJSON } = renderComponent(props)

        expect(toJSON()).toMatchSnapshot()
    })
})