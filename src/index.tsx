import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Provider } from 'mobx-react';
import { Text, View } from 'react-native';
import { routes } from './navigation/routes';
import Transaction from './screens/Transaction/Transaction';
import Transactions from './screens/Transactions/Transactions';
import { stores } from './store';

// TODO: better structure routing - ie. navigators into separate files

const Stack = createNativeStackNavigator()

const defaultHeader = (props: { children: string }) => (
  <View style={{ height: 40, justifyContent: 'center', alignItems: 'center' }}>
    <Text>{`${props.children[0].toUpperCase()}${props.children.slice(1)}`}</Text>
  </View>
)


export default function App() {
  return (
    <Provider {...stores}>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerTitle: defaultHeader, headerBackTitle: 'Back' }}>
          <Stack.Screen name={routes.transactions} component={Transactions} />
          <Stack.Screen name={routes.transaction} component={Transaction} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}