import { transactionsMock } from "../mocks/transactions"
import TransactionStore from "./transactions"

jest.mock('@react-native-async-storage/async-storage', () => ({
    setItem: jest.fn(),
    getItem: jest.fn(async () => {}),
}))

describe('stores/transactions', () => {
    const store = new TransactionStore()

    it('should fetch trasnactions', async () => {
        expect(store.transactions).toEqual([])

        await store.fetchTransactions()

        expect(store.transactions).toEqual(transactionsMock.transactions)
    })
})