import { action, observable } from 'mobx';
import { transactionsMock } from '../mocks/transactions';
import { Transaction } from '../types/Transaction';
import AsyncStorage from '@react-native-async-storage/async-storage'

export default class TransactionStore {
    @observable
    transactions: Transaction[] = [];

    @observable
    loading = false

    constructor() {
        // This should probably be a reaction when accessing a certain route
        AsyncStorage.getItem('transactions').then((txData) => {
            if (txData) {
                const txJSON = JSON.parse(txData)
                this.transactions = txJSON
            }
        })
    }

    public setTransactions(transactions: Transaction[]) {
        this.transactions = transactions;
        // TODO: ideally this shouldn't be here in order for the method
        // to actually have single responsibility + makes testing slightly complex 
        AsyncStorage.setItem('transactions', JSON.stringify(transactions))
    }

    // TODO: implement real fetch + pagination or infinite scroll handling
    @action
    async fetchTransactions() {
        this.loading = true
        try {
            await new Promise((res) => setTimeout(res, 250))
            this.setTransactions(transactionsMock.transactions)
        } catch (e) {
            // TODO: handle if request fails
            console.error('error', e)
        } finally {
            this.loading = false
        }
    }
}